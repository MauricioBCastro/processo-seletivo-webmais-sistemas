import "./css/tailwind.css";
import React, { useState, Component } from "react";
import Modal from "@material-tailwind/react/Modal";
import ModalHeader from "@material-tailwind/react/ModalHeader";
import ModalBody from "@material-tailwind/react/ModalBody";
import ModalFooter from "@material-tailwind/react/ModalFooter";
import Button from "@material-tailwind/react/Button";

export default function App() {
  const [showModal, setShowModal] = React.useState(false);

  return (
    <>
      <div>
        <div className="sm:px-6 w-full">
          <div className="px-4 md:px-10 py-4 md:py-7">
            <div className="flex items-center justify-between">
              <p className="text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800">
                Produtos
              </p>
            </div>
          </div>
          <div className="bg-white py-4 md:py-7 px-4 md:px-8 xl:px-10">
            <div className="sm:flex items-center justify-between">
              <div className="flex items-center">
                <a href="javascript:void(0)">
                  <div className="py-2 px-8 bg-indigo-100 text-indigo-700 rounded-full">
                    <p>All</p>
                  </div>
                </a>
              </div>
              <div></div>
              <Button
                color="lightBlue"
                type="button"
                onClick={(e) => setShowModal(true)}
                ripple="light"
              >
                Open small Modal
              </Button>
              <Modal
                size="sm"
                active={showModal}
                toggler={() => setShowModal(false)}
              >
                <ModalHeader toggler={() => setShowModal(false)}>
                  Novo produto
                </ModalHeader>
                <ModalBody>
                  <p className="text-base leading-relaxed text-gray-600 font-normal">
                    I always felt like I could do anything. That’s the main
                    thing people are controlled by! Thoughts- their perception
                    of themselves! They're slowed down by their perception of
                    themselves. If you're taught you can’t do anything, you
                    won’t do anything. I was taught I could do everything.
                  </p>
                </ModalBody>
                <ModalFooter>
                  <Button
                    color="red"
                    buttonType="link"
                    onClick={(e) => setShowModal(false)}
                    ripple="dark"
                  >
                    Close
                  </Button>

                  <Button
                    color="green"
                    onClick={(e) => setShowModal(false)}
                    ripple="light"
                  >
                    Save Changes
                  </Button>
                </ModalFooter>
              </Modal>
            </div>
            <div className="mt-7 overflow-x-auto">
              <table className="w-full whitespace-nowrap">
                <tbody>
                  <tr className="h-16 border border-gray-100 rounded">
                    <td></td>
                    <td className>
                      <div className="flex items-center pl-5">
                        <p className="text-lg font-medium leading-none text-gray-700 mr-2">
                          Produto 1
                        </p>
                      </div>
                    </td>
                    <td className="pl-24"></td>
                    <td className="pl-5">id produto</td>
                    <td className="pl-5">
                      <div className="flex items-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={20}
                          height={20}
                          viewBox="0 0 20 20"
                          fill="none"
                        >
                          <path
                            d="M3.33331 17.4998V6.6665C3.33331 6.00346 3.59671 5.36758 4.06555 4.89874C4.53439 4.4299 5.17027 4.1665 5.83331 4.1665H14.1666C14.8297 4.1665 15.4656 4.4299 15.9344 4.89874C16.4033 5.36758 16.6666 6.00346 16.6666 6.6665V11.6665C16.6666 12.3295 16.4033 12.9654 15.9344 13.4343C15.4656 13.9031 14.8297 14.1665 14.1666 14.1665H6.66665L3.33331 17.4998Z"
                            stroke="#52525B"
                            strokeWidth="1.25"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M10 9.1665V9.17484"
                            stroke="#52525B"
                            strokeWidth="1.25"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M6.66669 9.1665V9.17484"
                            stroke="#52525B"
                            strokeWidth="1.25"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M13.3333 9.1665V9.17484"
                            stroke="#52525B"
                            strokeWidth="1.25"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                        <p className="text-sm leading-none text-gray-600 ml-2">
                          Quantidade de comentarios
                        </p>
                      </div>
                    </td>
                    <td className="pl-1">
                      <button className="text-base leading-none text-gray-600 py-3 px-5 bg-gray-100 rounded hover:bg-gray-200 focus:outline-none">
                        Visualizar
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
